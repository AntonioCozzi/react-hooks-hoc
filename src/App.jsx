/**
 * @author Antonio Cozzi <antoniocozzi2@gmail.com>
 * @description Sample project which unifies ReactJS HOC and few use of React 16 Hooks.
 */

import React, { useState, useEffect } from 'react';
import { exFunc } from "./utils";

import './App.css';

/**
 * HOC
 * @description Takes a JSX Element and wraps around functionality
 */
const withValidation = (WrappedComponent, func) =>
  props => {
    const [error, setError] = useState(false)

    return (
      <div
        onChange={() => setError(false)}
        onBlur={ev => ev.target.value === '' && setError(true)}
        onFocus={() => func()}
        style={error ? { backgroundColor: "red" } : {}}
      >
        <WrappedComponent {...props} />
      </div>
    )
  }

// Components
const InputComponent = (props) => {
  const [content, setContent] = useState('')
  useEffect(() => props.customProp(content))

  const handleChange = content => {
    setContent(content)
  }

  return (
    <input type="text" onChange={ev => handleChange(ev.target.value)} />
  )
}



const EnanchedComponent = withValidation(InputComponent, exFunc)

function App() {
  const initialState = JSON.parse(localStorage.getItem("todos")) || []
  const [todos, setTodos] = useState(initialState)
  useEffect(() => localStorage.setItem("todos", JSON.stringify(todos)), [todos])
  const [content, setContent] = useState('')

  function handleData(value) {
    setContent(value)
  }

  return (
    <div>
      <h2>Todos</h2>
      <form onSubmit={ev => {
        ev.preventDefault()
        setTodos([...todos, { content }])
      }}>
        {/* HOC usage */}
        <EnanchedComponent customProp={handleData} />
        <button type="submit">Add todo</button>
      </form>
      {
        todos.length === 0 ?
          <p>This list is empty.</p>
          :
          <ul>
            {
              todos.map((todo, index) =>
                <li key={index}>
                  <p>{todo.content}</p>
                  <button onClick={() =>
                    setTodos([...todos.slice(0, index),
                    ...todos.slice(index + 1, todos.length)])
                  }>X</button>
                </li>
              )
            }
          </ul>
      }
    </div>
  );
}

export default App;
