import { createContext } from 'react'

const ContentContext = createContext();

export { ContentContext as default }